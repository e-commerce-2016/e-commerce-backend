package com.mobilizr.feedback.services;

import com.mobilizr.comment.dao.CommentDAO;
import com.mobilizr.comment.dao.CommentDAOImpl;
import com.mobilizr.comment.dao.pojos.Comment;
import com.mobilizr.shop.dao.ShopDAO;
import com.mobilizr.shop.dao.ShopDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.Calendar;

public class FeedbackService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(FeedbackService.class);

    public void storeFeedback(long shopId, long userId, String commentStr, long rating ) {

        if (rating < 1 || rating > 5 ) {
            throw new IllegalArgumentException ("Rating is out of range [1-5]");
        }

        CommentDAO comments = new CommentDAOImpl();
        ShopDAO shops = new ShopDAOImpl();

        // Get current date and time
        Calendar calendar = Calendar.getInstance();
        Date datetime = new Date(calendar.getTime().getTime());

        // Create new comment
        Comment comment = new Comment();
        comment.setUserId(userId);
        comment.setShopId(shopId);
        comment.setComment(commentStr);
        comment.setRating(rating);
        comment.setDatetime(datetime);

        // Persist new comment
        comments.addComment(comment);

        // Update ratings
        shops.updateRatings(shopId, (int) rating);
    }


}
