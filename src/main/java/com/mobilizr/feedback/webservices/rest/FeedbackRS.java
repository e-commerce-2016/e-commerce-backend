package com.mobilizr.feedback.webservices.rest;

import com.mobilizr.feedback.services.FeedbackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/feedback")
public class FeedbackRS {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackRS.class);

    @POST
    @Path("/{userId}/{shopId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postFeedback(
            @PathParam("shopId")  long shopId,
            @PathParam("userId")  long userId,
            @FormParam("comment") String comment,
            @FormParam("rating")  int rating)
    {
        FeedbackService feedbackService = new FeedbackService();
        feedbackService.storeFeedback(shopId, userId, comment, rating);

        LOGGER.info("Posting feedback for shop {} by user {} (rating: {}, comment: {})", shopId, userId, rating, comment);
        return Response.status(201).build();
    }

}
