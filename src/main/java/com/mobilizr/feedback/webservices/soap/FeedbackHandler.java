package com.mobilizr.feedback.webservices.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface FeedbackHandler {

    /**
     * Attempts to reduce quantity of shop has product record.
     * @param shopId
     * @param userId
     * @param comment
     * @param rating
     * @param realPrice
     * @return True on success, false otherwise.s
     */
    @WebMethod(action = "insertFeedbackAction")
    public boolean postFeedback(
            @WebParam(name = "shop") long shopId,
            @WebParam(name = "user") long userId,
            @WebParam(name = "comment") String comment,
            @WebParam(name = "rating") long rating,
            @WebParam(name = "realPrice") double realPrice);

}
