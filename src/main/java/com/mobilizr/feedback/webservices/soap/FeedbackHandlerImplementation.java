package com.mobilizr.feedback.webservices.soap;

import com.mobilizr.comment.dao.CommentDAO;
import com.mobilizr.comment.dao.CommentDAOImpl;
import com.mobilizr.comment.dao.pojos.Comment;
import com.mobilizr.shop.dao.ShopDAO;
import com.mobilizr.shop.dao.ShopDAOImpl;

import javax.jws.WebService;
import java.sql.Date;
import java.util.Calendar;

@WebService(endpointInterface = "com.mobilizr.feedback.webservices.soap.FeedbackHandler")
public class FeedbackHandlerImplementation
        implements FeedbackHandler {

    @Override
    public boolean postFeedback(long shopId, long userId, String comment, long rating, double realPrice) {

        if (rating < 1 || rating > 5 ) {
            throw new IllegalArgumentException ("Rating is out of range [1-5]");
        }

        CommentDAO comments = new CommentDAOImpl();
        ShopDAO shops = new ShopDAOImpl();

        // Get current date and time
        Calendar calendar = Calendar.getInstance();
        Date datetime = new Date(calendar.getTime().getTime());

        // Create new comment
        Comment commentIn = new Comment();
        commentIn.setUserId(1L); // The anonymous user
        commentIn.setShopId(shopId);
        commentIn.setComment(comment);
        commentIn.setRating(rating);
        commentIn.setRealPrice(realPrice);
        commentIn.setDatetime(datetime);

        // Persist new comment
        boolean persisted = comments.addComment(commentIn);

        // Update ratings
        shops.updateRatings(shopId, (int) rating);

        return persisted;
    }

}
