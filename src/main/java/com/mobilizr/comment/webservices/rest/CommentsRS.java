package com.mobilizr.comment.webservices.rest;

import com.mobilizr.comment.dao.CommentDAO;
import com.mobilizr.comment.dao.CommentDAOImpl;
import com.mobilizr.comment.dao.pojos.Comment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/comments")
public class CommentsRS {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommentsRS.class);

    @GET
    @Path("/{shopId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getComments(@PathParam("shopId") long shopId) {
        CommentDAO commentDAO = new CommentDAOImpl();
        List<Comment> commentsList = commentDAO.getRelatedComments(shopId);

        // Notice the "{}" - We're creating a (typically anonymous) subclass of GenericEntity
        // More here: http://docs.oracle.com/javaee/6/api/javax/ws/rs/core/GenericEntity.html
        GenericEntity<List<Comment>> entity = new GenericEntity<List<Comment>>(commentsList) {};

        LOGGER.info("Response with {} comments for shopID({})", commentsList.size(), shopId);
        return Response.status(200).entity(entity).build();
    }
}


