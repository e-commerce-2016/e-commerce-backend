package com.mobilizr.comment.dao.pojos;

import java.sql.Date;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Comment {
    private Long id;
    private Long user_id;
    private String comment;
    private Long rating;
    private Date datetime;
    private Long shop_id;
    private double realPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public Long getUserId() {
        return user_id;
    }

    public void setUserId(Long user_id) {

        this.user_id = user_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {

        this.comment = comment;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;

    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Long getShopId() {
        return shop_id;
    }

    public void setShopId(Long shop_id) {
        this.shop_id = shop_id;
    }

    public double getRealPrice (){
        return realPrice;
    }
    public void setRealPrice(double realPrice) {
        this.realPrice = realPrice;
    }
}
