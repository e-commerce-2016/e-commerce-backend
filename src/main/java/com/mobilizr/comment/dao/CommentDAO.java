package com.mobilizr.comment.dao;

import com.mobilizr.comment.dao.pojos.Comment;
import com.mobilizr.dbutil.exceptions.DAOException;

import java.util.List;

public interface CommentDAO {
    /**
     * Finds the comments related to the given shopID parameter
     * @param    shopID     The search key
     * @return              All the comments that match the search query
     */
    public List<Comment> getRelatedComments(Long shopID) throws DAOException;

    /**
     * Stores a comment
     * @param   comment     The comment to be stored
     * @return              True if the comment was added to the database
     */
    public boolean addComment (Comment comment);
}
