package com.mobilizr.comment.dao;

import com.mobilizr.comment.dao.pojos.Comment;
import com.mobilizr.dbutil.DAOFactory;
import com.mobilizr.dbutil.exceptions.DAOException;
//import com.mobilizr.shop_related.dao.wrapper.ShopWithCommentDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.mobilizr.dbutil.DAOUtil.prepareStatement;


public class CommentDAOImpl implements CommentDAO{

    // Constants -------------------------------------------------------------------------------------------------------
    private static final String SQL_FIND_COMMENTS_BY_SHOP =
            "SELECT c.id, c.user_id, c.shop_id, c.comment, c.rating, c.datetime, c.realPrice FROM comment c WHERE shop_id = ?";

    private static final String SQL_INSERT_COMMENT =
            "INSERT INTO comment (" +
            "user_id, " +
            "shop_id, " +
            "comment, " +
            "rating, " +
            "datetime, " +
            "realPrice " +
            ") VALUES (?,?,?,?,?,?)";

    // Fields ----------------------------------------------------------------------------------------------------------
    private static final Logger LOGGER = LoggerFactory.getLogger(CommentDAO.class);
    private DataSource dataSource = DAOFactory.getDataSourceInstance();

    // Actions ---------------------------------------------------------------------------------------------------------
    @Override
    public List<Comment> getRelatedComments(Long shopID) {
        List<Comment> relatedComments = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = prepareStatement(connection, SQL_FIND_COMMENTS_BY_SHOP, false, shopID);
                ResultSet resultSet = preparedStatement.executeQuery();
        )
        {
            while (resultSet.next()) {
                relatedComments.add(map(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DAOException("Failure fetching related products with query: \"" + shopID + "\"");
        }
        return relatedComments;
    }

    @Override
    public boolean addComment (Comment comment) {

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement =
                        prepareStatement(connection, SQL_INSERT_COMMENT, false,
                                comment.getUserId(),
                                comment.getShopId(),
                                comment.getComment(),
                                comment.getRating(),
                                comment.getDatetime(),
                                comment.getRealPrice());
        )
        {
            return preparedStatement.executeUpdate() == 1;
        }
        catch (SQLException e) {
            throw new DAOException("Failure in adding comment: \"" + comment.getUserId() + " " + comment.getShopId() + " "
                                                                   + comment.getComment() + " " + comment.getRating() + " "
                                                                                                + comment.getRealPrice() +"\"");
        }
    }


    // Helpers ---------------------------------------------------------------------------------------------------------
    /**
     * Map the current row of the given ResultSet to a Comment.
     * @param resultSet The ResultSet of which the current row is to be mapped to a Comment.
     * @return The mapped Comment from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Comment map(ResultSet resultSet) throws SQLException {
        Comment comment = new Comment();
        comment.setId(resultSet.getLong("id"));
        comment.setUserId(resultSet.getLong("user_id"));
        comment.setShopId(resultSet.getLong("shop_id"));
        comment.setComment(resultSet.getString("comment"));
        comment.setRating(resultSet.getLong("rating"));
        comment.setDatetime(resultSet.getDate("datetime"));
        comment.setRealPrice(resultSet.getDouble("realPrice"));
        return comment;
    }

}
