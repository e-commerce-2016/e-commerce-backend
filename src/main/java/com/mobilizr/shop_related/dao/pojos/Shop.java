package com.mobilizr.shop_related.dao.pojos;

public class Shop {
  private Long id;
  private String brand;
  private Double lat;
  private Double lon;
  private String address;
  private String city;
  private String postal_code;
  private String phone;
  private String mobile_phone;
  private String trn;
  private Long rating_count;
  private Long rating_sum;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLon() {
    return lon;
  }

  public void setLon(Double lon) {
    this.lon = lon;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPostal_code() {
    return postal_code;
  }

  public void setPostal_code(String postal_code) {
    this.postal_code = postal_code;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getMobile_phone() {
    return mobile_phone;
  }

  public void setMobile_phone(String mobile_phone) {
    this.mobile_phone = mobile_phone;
  }

  public String getTrn() {
    return trn;
  }

  public void setTrn(String trn) {
    this.trn = trn;
  }

  public Long getRating_count() {
    return rating_count;
  }

  public void setRating_count(Long rating_count) {
    this.rating_count = rating_count;
  }

  public Long getRating_sum() {
    return rating_sum;
  }

  public void setRating_sum(Long rating_sum) {
    this.rating_sum = rating_sum;
  }
}
