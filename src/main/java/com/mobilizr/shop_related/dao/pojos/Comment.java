package com.mobilizr.shop_related.dao.pojos;

public class Comment {
  private Long id;
  private Long user_id;
  private String comment;
  private Long rating;
  private java.sql.Date datetime;
  private Long shop_id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUser_id() {
    return user_id;
  }

  public void setUser_id(Long user_id) {
    this.user_id = user_id;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Long getRating() {
    return rating;
  }

  public void setRating(Long rating) {
    this.rating = rating;
  }

  public java.sql.Date getDatetime() {
    return datetime;
  }

  public void setDatetime(java.sql.Date datetime) {
    this.datetime = datetime;
  }

  public Long getShop_id() {
    return shop_id;
  }

  public void setShop_id(Long shop_id) {
    this.shop_id = shop_id;
  }
}
