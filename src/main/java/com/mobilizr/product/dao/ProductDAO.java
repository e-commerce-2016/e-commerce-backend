package com.mobilizr.product.dao;

import java.util.List;
import com.mobilizr.dbutil.exceptions.DAOException;
import com.mobilizr.product.dao.pojos.Product;

public interface ProductDAO {
    /**
     * Finds the products related to the given productName parameter
     * @param productName   The search key
     * @return              All the products that match the search query, along with their prices
     */
    public List<Product> getRelatedProducts(String productName) throws DAOException;
}
