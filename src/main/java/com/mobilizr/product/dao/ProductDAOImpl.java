package com.mobilizr.product.dao;

import static com.mobilizr.dbutil.DAOUtil.*;

import com.mobilizr.dbutil.DAOFactory;
import com.mobilizr.dbutil.exceptions.DAOException;
import com.mobilizr.product.dao.pojos.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

public class ProductDAOImpl implements ProductDAO {
    // Constants -------------------------------------------------------------------------------------------------------
    private static final String SQL_FIND_BY_NAME =
            "SELECT id, name, image, description FROM product WHERE name LIKE ?";

    // Fields ----------------------------------------------------------------------------------------------------------
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDAO.class);
    private DataSource dataSource = DAOFactory.getDataSourceInstance();

    // Actions ---------------------------------------------------------------------------------------------------------
    @Override
    public List<Product> getRelatedProducts(String productName) {
        List<Product> relatedProducts = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = prepareStatement(connection, SQL_FIND_BY_NAME, false, productName);
                ResultSet resultSet = preparedStatement.executeQuery();
                )
        {
            while (resultSet.next()) {
                relatedProducts.add(map(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DAOException("Failure fetching related products with query: \"" + productName + "\"");
        }
        return relatedProducts;
    }

    // Helpers ---------------------------------------------------------------------------------------------------------
    /**
     * Map the current row of the given ResultSet to a Product.
     * @param resultSet The ResultSet of which the current row is to be mapped to a Product.
     * @return The mapped Product from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Product map(ResultSet resultSet) throws SQLException {
        Product product = new Product();
        product.setId(resultSet.getLong("id"));
        product.setName(resultSet.getString("name"));
        product.setDescription(resultSet.getString("description"));
        product.setImage(resultSet.getString("image"));
        return product;
    }
}
