package com.mobilizr.product.services;


import com.mobilizr.MobilizrProperties;
import com.mobilizr.product.dao.ProductDAO;
import com.mobilizr.product.dao.ProductDAOImpl;
import com.mobilizr.product.dao.pojos.Product;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class ProductsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductsService.class);

    public List<Product> getProducts(String productName) {
        productName = "%" + productName + "%";

        ProductDAO productDAO = new ProductDAOImpl();
        List<Product> allProducts = productDAO.getRelatedProducts(productName);
        String applicationDataPath = MobilizrProperties.getApplicationDataPath()
                + MobilizrProperties.getMediaDataPath();

        // Leaving timers in comments, as we might need to re-evaluate this.
//        long startTime, endTime, totalStarTime = System.nanoTime(), totalEndTime;
        for (Product product : allProducts) {
            String imageFilename = applicationDataPath + product.getImage();
            try {
//                startTime = System.nanoTime();
                File imageFile = new File(imageFilename);
                byte[] imageBytes = Files.readAllBytes(imageFile.toPath());
                imageBytes = Base64.encodeBase64(imageBytes);
                product.setImage(new String(imageBytes));
//                endTime = System.nanoTime();
//                LOGGER.info("\tConversion took: " + (endTime - startTime)/1000000 + "ms");
            } catch (FileNotFoundException e) {
                // Doesn't matter, reply with the product and an empty image String.
                LOGGER.warn("Image \"{}\" was not found.", imageFilename);
                product.setImage("");
            } catch (IOException e) {
                // Doesn't matter, reply with the product and an empty image String.
                LOGGER.warn("Image \"{}\" could not be read.", imageFilename);
                product.setImage("");
            }
        }
//        totalEndTime = System.nanoTime();
//        LOGGER.info("Reply took: " + (totalEndTime - totalStarTime)/1000000 + "ms");

        return allProducts;
    }
}
