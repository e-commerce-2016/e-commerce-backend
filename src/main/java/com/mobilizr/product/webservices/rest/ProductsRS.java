package com.mobilizr.product.webservices.rest;

import com.mobilizr.product.dao.pojos.Product;
import com.mobilizr.product.services.ProductsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/products")
public class ProductsRS {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductsRS.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProducts(
            @DefaultValue("") @QueryParam("keyphrase") @Size(max = 128) String productName) {
        // Don't search db with empty keyphrase
        if (productName.equals("")) {
            LOGGER.info("Received empty keyphrase.");
            throw new WebApplicationException(
                    Response.status(HttpServletResponse.SC_BAD_REQUEST)
                            .entity("Bad request: Use a parameter for searching a product, e.g.: " +
                                    "\"?keyphrase=something\"").build()
            );
        }
        ProductsService productsService = new ProductsService();
        List<Product> allProducts = productsService.getProducts(productName);
        GenericEntity<List<Product>> entity =
                new GenericEntity<List<Product>>(allProducts) {
                };
        return Response.status(200).entity(entity).build();
    }

}
