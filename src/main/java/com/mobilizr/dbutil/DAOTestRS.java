package com.mobilizr.dbutil;

import com.mobilizr.MobilizrProperties;
import com.mobilizr.comment.dao.CommentDAOImpl;
import com.mobilizr.comment.dao.pojos.Comment;
import com.mobilizr.product.dao.ProductDAOImpl;
import com.mobilizr.product.dao.pojos.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Simple RS for testing connection with the DB.
 */
@Path("/hello")
public class DAOTestRS {
    private static final Logger LOGGER = LoggerFactory.getLogger(DAOTestRS.class);

    @GET
    public Response getMsg() {
        String output = "Jersey said hey";
        ProductDAOImpl productDAO = new ProductDAOImpl();
        //LOGGER.info(MobilizrProperties.getApplicationDataPath());
        List<Product> relatedProducts = productDAO.getRelatedProducts("Nexus%");
        for (Product product : relatedProducts)
            LOGGER.info("Sending product: ID({})\tNAME({})\tDESC({})\n",
                    product.getId(), product.getName(), product.getDescription());
        // TODO Remove info code and return products in JSON format (using JAXB).
        return Response.status(200).entity(output).build();
    }
}