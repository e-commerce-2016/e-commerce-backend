package com.mobilizr.dbutil;

import com.mobilizr.dbutil.exceptions.DAOConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * DAOFactory as demonstrated in BalusC's blog post
 * <a href="http://balusc.omnifaces.org/2008/07/dao-tutorial-data-layer.html">here.</a>
 * It's actually a combination of DAOFactory and the DataSource based DAOFactory (since we only need this one).
 *
 * Following fast, thread-safe singleton pattern, as described
 * <a href="http://stackoverflow.com/questions/70689/what-is-an-efficient-way-to-implement-a-singleton-pattern-in-java?rq=1">here.</a>
 * (Not using the ENUM solution, as it hides some details, clearly visible by this one.)
 *
 * DAO_X_Implementation classes will have a reference to the DataSource provided by this singleton.
 */
public final class DAOFactory {
    // Fields ----------------------------------------------------------------------------------------------------------
    private static final Logger LOGGER = LoggerFactory.getLogger(DAOFactory.class);
    // Singleton field
    private DataSource dataSource = null;

    // Constructor -----------------------------------------------------------------------------------------------------
    private DAOFactory() {
        String datasourceJNDI = "java:comp/env/jdbc/mobilizrDatasource";
        try {
            dataSource = (DataSource) new InitialContext().lookup(datasourceJNDI);
            LOGGER.info("Acquired DataSource via JNDI");
        } catch (NamingException e) {
            throw new DAOConfigurationException("DataSource '" + datasourceJNDI + "' is missing in JNDI.", e);
        }
    }

    // This static loader class will hold our DAOFactory singleton.
    // This is ensured by Java Semantics to be created only once, when the class is referred to
    // (lazy initialization of the singleton).
    private static class DAOFactoryLoader {
        private static final DAOFactory INSTANCE = new DAOFactory();
    }

    // Actions ---------------------------------------------------------------------------------------------------------
    public static DataSource getDataSourceInstance() {
        return DAOFactoryLoader.INSTANCE.dataSource;
    }
}
