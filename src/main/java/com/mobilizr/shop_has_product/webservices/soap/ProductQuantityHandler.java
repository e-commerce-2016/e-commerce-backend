package com.mobilizr.shop_has_product.webservices.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ProductQuantityHandler {

    /**
     * Attempts to reduce quantity of shop has product record.
     * @param shopId
     * @param productId
     * @return True on success, false otherwise.s
     */
    @WebMethod(action = "reduceQuantityAction")
    public boolean reduceQuantity(
            @WebParam(name = "shop") long shopId,
            @WebParam(name = "product") long productId);

    /**
     * Attempts to increase quantity of shop has product record.
     * @param shopId
     * @param productId
     * @return True on success, false otherwise.s
     */
    @WebMethod(action = "increaseQuantityAction")
    public boolean increaseQuantity(
            @WebParam(name = "shop") long shopId,
            @WebParam(name = "product") long productId);
}
