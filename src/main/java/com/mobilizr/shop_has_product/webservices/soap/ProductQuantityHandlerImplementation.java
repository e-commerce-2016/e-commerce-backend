package com.mobilizr.shop_has_product.webservices.soap;

import com.mobilizr.shop_has_product.dao.ShopHasProductDAO;
import com.mobilizr.shop_has_product.dao.ShopHasProductDAOImpl;

import javax.jws.WebService;

@WebService(endpointInterface = "com.mobilizr.shop_has_product.webservices.soap.ProductQuantityHandler")
public class ProductQuantityHandlerImplementation
        implements ProductQuantityHandler {
    @Override
    public boolean reduceQuantity(long shopId, long productId) {

        ShopHasProductDAO shopHasProductDAO = new ShopHasProductDAOImpl();

        return shopHasProductDAO.decrementProductQuantity(shopId, productId);
    }
    @Override
    public boolean increaseQuantity(long shopId, long productId){

        ShopHasProductDAO shopHasProductDAO = new ShopHasProductDAOImpl();
        
        return shopHasProductDAO.incrementProductQuantity(shopId, productId);
    }
}
