package com.mobilizr.shop_has_product.webservices.rest;

import com.mobilizr.shop_has_product.dao.pojos.ShopHasProductExtended;
import com.mobilizr.shop_has_product.services.ShopHasProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/shophasproduct")
public class ShopHasProductRS {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShopHasProductRS.class);

    @GET
    @Path("/{productId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getShop(@PathParam("productId") long productId)
    {
        ShopHasProductService shopsService = new ShopHasProductService();
        List<ShopHasProductExtended> shops = shopsService.getProductShops(productId);

        // Notice the "{}" - We're creating a (typically anonymous) subclass of GenericEntity
        // More here: http://docs.oracle.com/javaee/6/api/javax/ws/rs/core/GenericEntity.html
        GenericEntity<List<ShopHasProductExtended>> entity =
                new GenericEntity<List<ShopHasProductExtended>>(shops) {};

        LOGGER.info("Response with {} shops for productID({})", shops.size(), productId);
        return Response.status(200).entity(entity).build();
    }
}
