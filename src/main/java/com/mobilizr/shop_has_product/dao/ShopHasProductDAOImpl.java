package com.mobilizr.shop_has_product.dao;

import com.mobilizr.dbutil.DAOFactory;
import com.mobilizr.dbutil.exceptions.DAOException;
//import com.mobilizr.shop_related.dao.pojos.Shop;
import com.mobilizr.shop_has_product.dao.pojos.Shop_has_product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.mobilizr.dbutil.DAOUtil.prepareStatement;


public class ShopHasProductDAOImpl implements ShopHasProductDAO{

    private static final String SQL_FIND_SHOPS_BY_PRODUCT =
            //"SELECT shop.id, shop.brand, shop.lat, shop.lon, shop.address, shop.city, shop.rating_count, shop.rating_sum FROM shop INNER JOIN shop_has_product ON shop.id = shop_has_product.shop_id WHERE shop_has_product.products_id = ?";
            "SELECT * FROM shop_has_product WHERE products_id = ?";

    private static final String SQL_INCREMENT_QUANTITY =
            "UPDATE shop_has_product SET quantity = quantity + 1 " +
                    "WHERE shop_id = ? AND products_id = ?";

    private static final String SQL_DECREMENT_QUANTITY =
            "UPDATE shop_has_product SET quantity = quantity - 1 " +
                    "WHERE shop_id = ? AND products_id = ? AND quantity > 0";

    private static final String SQL_FIND_SHOP_HAS_PRODUCT =
            "SELECT * FROM shop_has_product WHERE shop_id = ? AND products_id = ?";

    // Fields ----------------------------------------------------------------------------------------------------------
    private static final Logger LOGGER = LoggerFactory.getLogger(ShopHasProductDAO.class);
    private DataSource dataSource = DAOFactory.getDataSourceInstance();

    // Actions ---------------------------------------------------------------------------------------------------------

    @Override
    public List<Shop_has_product> getRelatedShopsByProduct(Long productID) {
        List<Shop_has_product> relatedShops = new ArrayList<>();

        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = prepareStatement(connection, SQL_FIND_SHOPS_BY_PRODUCT, false, productID);
                ResultSet resultSet = preparedStatement.executeQuery();
        )
        {
            while (resultSet.next()) {
                relatedShops.add(mapShop(resultSet));
            }
        }
        catch (SQLException e) {
            throw new DAOException("Failure fetching related shops with query: \"" + productID + "\"");
        }
        return relatedShops;
    }

    public Shop_has_product getShopHashProduct(long shopId, long productId)
            throws SQLException
    {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = prepareStatement(connection, SQL_FIND_SHOP_HAS_PRODUCT, false, shopId, productId);
                ResultSet resultSet = preparedStatement.executeQuery();
        )
        {
            if (resultSet.next()) {
                return mapShop(resultSet);
            }
            return null;
        }
    }

    @Override
    public boolean incrementProductQuantity(long shopId, long productId) {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = prepareStatement(connection, SQL_INCREMENT_QUANTITY, false, shopId, productId);
        )
        {
            int rowCount = preparedStatement.executeUpdate();

            if (rowCount != 1) {
                throw new DAOException("Failure updating product quantity: shopID = "
                        + shopId + ", productID = " + productId);
            }
            return true;

        }
        catch (SQLException e) {
            throw new DAOException("Failure updating product quantity: shopID = "
                    + shopId + ", productID = " + productId);
        }
    }

    @Override
    public boolean decrementProductQuantity(long shopId, long productId) {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = prepareStatement(connection, SQL_DECREMENT_QUANTITY, false, shopId, productId);
        )
        {
            int rowCount = preparedStatement.executeUpdate();

            if (rowCount != 1) {
                // Check if ShopHasProduct exists
                if (getShopHashProduct(shopId, productId) != null) {
                    return false;   // zero quantity
                }
                throw new DAOException("Failure updating product quantity: shopID = "
                        + shopId + ", productID = " + productId);
            }
            return true;

        }
        catch (SQLException e) {
            throw new DAOException("Failure updating product quantity: shopID = "
                    + shopId + ", productID = " + productId);
        }
    }

    // Helpers ---------------------------------------------------------------------------------------------------------
    /**
     * Map the current row of the given ResultSet to a Product.
     * @param resultSet The ResultSet of which the current row is to be mapped to a Product.
     * @return The mapped Product from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    private static Shop_has_product mapShop(ResultSet resultSet) throws SQLException {
        Shop_has_product shopHasProduct = new Shop_has_product();
        shopHasProduct.setId(resultSet.getLong("id"));
        shopHasProduct.setShop_id(resultSet.getLong("shop_id"));
        shopHasProduct.setProducts_id(resultSet.getLong("products_id"));
        shopHasProduct.setPrice(resultSet.getDouble("price"));
        shopHasProduct.setQuantity(resultSet.getLong("quantity"));
        return shopHasProduct;
    }
}