package com.mobilizr.shop_has_product.dao;


import com.mobilizr.dbutil.exceptions.DAOException;
import com.mobilizr.shop_has_product.dao.pojos.Shop_has_product;

import java.util.List;

public interface ShopHasProductDAO {
    /**
     * Finds the products related to the given productName parameter
     * @param productID     the search key
     * @return              All the shops that match the search query, along with their values
     */
    public List<Shop_has_product> getRelatedShopsByProduct(Long productID) throws DAOException;

    /**
     * Increase product quantity by 1
     * @param shopId        The shop that contains the product
     * @param productId     The product whose quantity we are increasing
     * @return              True if quantity was successfully incremented
     */
    public boolean incrementProductQuantity(long shopId, long productId);

    /**
     * Reduce product quantity by 1
     * @param shopId        The shop that contains the product
     * @param productId     The product whose quantity we are reducing
     * @return              True if quantity was successfully decremented
     */
    public boolean decrementProductQuantity(long shopId, long productId);
}
