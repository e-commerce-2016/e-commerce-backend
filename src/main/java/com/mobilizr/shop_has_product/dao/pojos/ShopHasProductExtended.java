package com.mobilizr.shop_has_product.dao.pojos;

import com.mobilizr.shop.dao.pojos.Shop;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class ShopHasProductExtended
{
    private Shop_has_product shopHasProduct;
    private Shop shop;

    public ShopHasProductExtended(Shop_has_product shopHasProduct, Shop shop)
    {
        if (shop.getId() != shopHasProduct.getShop_id())
            throw new IllegalArgumentException();

        this.shopHasProduct = shopHasProduct;
        this.shop = shop;
    }

    /** Empty constructor is required for serialization to work */
    public ShopHasProductExtended() {}

    public Shop getShop() {
        return shop;
    }

    /** Setter method for shop field */
    public void setShop(Shop shop) {
        this.shop = shop;
    }

    /** Setter method for shopHasProduct field */
    public void setShopHasProduct(Shop_has_product shopHasProduct) {
        this.shopHasProduct = shopHasProduct;
    }

    @XmlTransient
    public Long getShopId() {
        return shop.getId();
    }

    @XmlAttribute
    public Long getId() {
        return shopHasProduct.getId();
    }

    @XmlAttribute
    public Long getProductId() {
        return shopHasProduct.getProducts_id();
    }

    @XmlAttribute
    public Double getPrice() {
        return shopHasProduct.getPrice();
    }

    @XmlAttribute
    public Long getQuantity() {
        return shopHasProduct.getQuantity();
    }
}
