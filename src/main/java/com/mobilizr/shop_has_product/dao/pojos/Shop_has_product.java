package com.mobilizr.shop_has_product.dao.pojos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Shop_has_product {
  private Long id;
  private Long shop_id;
  private Long products_id;
  private Double price;
  private Long quantity;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getShop_id() {
    return shop_id;
  }

  public void setShop_id(Long shop_id) {
    this.shop_id = shop_id;
  }

  public Long getProducts_id() {
    return products_id;
  }

  public void setProducts_id(Long products_id) {
    this.products_id = products_id;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Long getQuantity() {
    return quantity;
  }

  public void setQuantity(Long quantity) {
    this.quantity = quantity;
  }
}
