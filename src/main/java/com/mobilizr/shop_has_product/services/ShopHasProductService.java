package com.mobilizr.shop_has_product.services;

import com.mobilizr.MobilizrProperties;
import com.mobilizr.shop.dao.ShopDAO;
import com.mobilizr.shop.dao.ShopDAOImpl;
import com.mobilizr.shop.dao.pojos.Shop;
import com.mobilizr.shop_has_product.dao.ShopHasProductDAO;
import com.mobilizr.shop_has_product.dao.ShopHasProductDAOImpl;
import com.mobilizr.shop_has_product.dao.pojos.ShopHasProductExtended;
import com.mobilizr.shop_has_product.dao.pojos.Shop_has_product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ShopHasProductService {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(ShopHasProductService.class);

    public List<ShopHasProductExtended> getProductShops(long productId)
    {
        ShopHasProductDAO shopHasProductDAO = new ShopHasProductDAOImpl();
        ShopDAO shopDAO = new ShopDAOImpl();

        // Get list of related shops for this product
        List<Shop_has_product> relatedShops =
                shopHasProductDAO.getRelatedShopsByProduct(productId);

        String applicationDataPath = MobilizrProperties.getApplicationDataPath()
                + MobilizrProperties.getMediaDataPath();

        // List with concrete shops
        List<ShopHasProductExtended> productShops = new ArrayList<>();

        for (Shop_has_product shopHasProduct : relatedShops) {
            // Get shop entity by its ID
            long shopId = shopHasProduct.getShop_id();
            Shop shop = shopDAO.getShop(shopId);

            productShops.add(new ShopHasProductExtended(shopHasProduct, shop));
        }

        return productShops;
    }
}
