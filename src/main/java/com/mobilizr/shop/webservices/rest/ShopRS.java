package com.mobilizr.shop.webservices.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mobilizr.shop.dao.ShopDAO;
import com.mobilizr.shop.dao.ShopDAOImpl;
import com.mobilizr.shop.dao.pojos.Shop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/shop")
public class ShopRS {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShopRS.class);

    @GET
    @Path("/{shopId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getShop(@PathParam("shopId") long shopId) {
        ShopDAO shopDAO = new ShopDAOImpl();
        Shop shop = shopDAO.getShop(shopId);

        // Notice the "{}" - We're creating a (typically anonymous) subclass of GenericEntity
        // More here: http://docs.oracle.com/javaee/6/api/javax/ws/rs/core/GenericEntity.html
        GenericEntity<Shop> entity = new GenericEntity<Shop>(shop) {};

        LOGGER.info("Response with {} shop for shopID({})", shop, shopId);
        return Response.status(200).entity(entity).build();
    }
}
