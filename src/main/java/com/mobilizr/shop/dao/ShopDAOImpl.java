package com.mobilizr.shop.dao;

import com.mobilizr.MobilizrProperties;
import com.mobilizr.dbutil.DAOFactory;
import com.mobilizr.dbutil.exceptions.DAOException;
import com.mobilizr.shop.dao.pojos.Shop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sql.DataSource;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.mobilizr.dbutil.DAOUtil.prepareStatement;


public class ShopDAOImpl implements ShopDAO {

    // Constants ---------------------------------------------------------------------------------------
    private static final String SQL_FIND_RELATED_SHOP =
            "SELECT * FROM shop WHERE id = ?";

    private static final String SQL_UPDATE_SHOP_RATINGS =
            "UPDATE shop SET rating_count = rating_count + 1, rating_sum = rating_sum + ? WHERE id = ?";

    // Fields ------------------------------------------------------------------------------------------

    private static final Logger LOGGER = LoggerFactory.getLogger(ShopDAO.class);
    private DataSource dataSource = DAOFactory.getDataSourceInstance();

    // Actions ------------------------------------------------------------------------------------------
    @Override
    public Shop getShop(Long shopId) {
        Shop relatedShop = new Shop();
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = prepareStatement(connection, SQL_FIND_RELATED_SHOP, false, shopId);
                ResultSet resultSet = preparedStatement.executeQuery();
        )
        {
            if (resultSet.next()) {
                relatedShop = map(resultSet);

                // At most one item should be returned
                assert !resultSet.next();
            } else {
                throw new DAOException(
                        "Failure fetching related shop with query: \""
                                + shopId + "\"");
            }
        }
        catch (SQLException e) {
            throw new DAOException(
                    "Failure fetching related shop with query: \""
                            + shopId + "\"");
        }
        return relatedShop;
    }

    @Override
    public void updateRatings(long shopId, int rating) throws DAOException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = prepareStatement(connection, SQL_UPDATE_SHOP_RATINGS, false, rating, shopId);
        )
        {
            // Update ratings for shop
            int rowCount = preparedStatement.executeUpdate();

            if (rowCount == 0)
                throw new DAOException("Invalid shop ID: " + shopId);
        }
        catch (SQLException e) {
            throw new DAOException(
                    "Failure in updating ratings for shop: \""
                            + shopId + "\"");
        }
    }

//    public Shop getShop(Long shopID){
//        Shop relatedShop = new Shop();
//        try (
//                Connection connection = dataSource.getConnection();
//                PreparedStatement preparedStatement = prepareStatement(connection, SQL_FIND_RELATED_SHOP, false, shopID);
//                ResultSet resultSet = preparedStatement.executeQuery();
//        )
//        {
//                relatedShop=map(resultSet);
//        }
//        catch (SQLException e) {
//            throw new DAOException("Failure fetching related shop with query: \"" + shopID + "\"");
//        }
//        return relatedShop;
//
//    }

    private static Shop map(ResultSet resultSet) throws SQLException {
        Shop shop = new Shop();
        shop.setId(resultSet.getLong("id"));
        shop.setBrand(resultSet.getString("brand"));
        shop.setLat(resultSet.getDouble("lat"));
        shop.setLon(resultSet.getDouble("lon"));
        shop.setAddress(resultSet.getString("address"));
        shop.setCity(resultSet.getString("city"));
        shop.setPostal_code(resultSet.getString("postal_code"));
        shop.setPhone(resultSet.getString("phone"));
        shop.setMobile_phone(resultSet.getString("mobile_phone"));
        shop.setTrn(resultSet.getString("trn"));
        shop.setRating_count(resultSet.getLong("rating_count"));
        shop.setRating_sum(resultSet.getLong("rating_sum"));
        return shop;
    }
}
