package com.mobilizr.shop.dao;

import com.mobilizr.dbutil.exceptions.DAOException;
import com.mobilizr.shop.dao.pojos.Shop;

public interface ShopDAO {
    /**
     * Finds the Shop related to the given productName parameter
     * @param shopID   The search key
     * @return         The shop that match the search query, along with their attributes
     */
    public Shop getShop(Long shopID) throws DAOException;

    /**
     * Upadates the shop ratings
     * @param shopId    The id of the shop that is being rated
     * @param rating    A new rating for the shop
     */
    public void updateRatings(long shopId, int rating) throws DAOException;
}
