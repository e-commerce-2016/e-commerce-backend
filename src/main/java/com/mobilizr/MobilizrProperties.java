package com.mobilizr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public final class MobilizrProperties {
    static String ApplicationPath;
    static String MediaPath;
    private static Properties prop = new Properties();

    private MobilizrProperties() {
    }

        static {
            try(InputStream input = MobilizrProperties.class.getClassLoader().getResourceAsStream("config.properties")) {
                final Logger LOGGER = LoggerFactory.getLogger(MobilizrProperties.class);
                //input = this.getClass().getResourceAsStream("config.properties");
                prop.load(input);
                ApplicationPath = System.getProperty("user.home") + prop.getProperty("application_data");
                LOGGER.info(prop.getProperty("application_data"));
                MediaPath = prop.getProperty("images_data");
                LOGGER.info(prop.getProperty("images_data"));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    public static String getApplicationDataPath() {

        if (ApplicationPath != null) {
            return ApplicationPath;
        } else {
            return null;
        }
    }

    public static String getMediaDataPath() {

        if (MediaPath != null) {
            return MediaPath;
        } else {
            return null;
        }
    }

}
